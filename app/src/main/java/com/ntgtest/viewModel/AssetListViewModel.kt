package com.ntgtest.viewModel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.ntgtest.model.AssetModel
import com.ntgtest.model.db.DataBaseHandler
import com.ntgtest.model.db.asset.Asset
import com.ntgtest.utils.Event
import com.ntgtest.view.assets_list.validation.AssetValidation
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class AssetListViewModel(
    private val dataBaseHandler: DataBaseHandler,
) : ViewModel() {

    val items = arrayListOf<AssetModel>()

    val addAssetValidation = MutableLiveData<Event<ArrayList<AssetValidation>>>()
    fun validateAsset(model: AssetModel) {
        val validationEvents = arrayListOf<AssetValidation>()
        var valid = true
        if (model.barCode.isBlank()) {
            validationEvents.add(AssetValidation.BARCODE_EMPTY)
            valid = false
        }
        if (model.category.isBlank()) {
            validationEvents.add(AssetValidation.CATEGORY_EMPTY)
            valid = false
        }
        if (model.description.isBlank()) {
            validationEvents.add(AssetValidation.DESCRIPTION_EMPTY)
            valid = false
        }
        if (valid)
            viewModelScope.launch(Dispatchers.IO) {
                if (!dataBaseHandler.hasBarcode(model.barCode)) {
                    addAsset(model)
                } else
                    validationEvents.add(AssetValidation.BARCODE_NOT_UNIQUE)
                addAssetValidation.postValue(Event(validationEvents))
            }
        else
            addAssetValidation.postValue(Event(validationEvents))
    }

    val addAssetObserver = MutableLiveData<Event<Boolean>>()
    private fun addAsset(model: AssetModel) {
        viewModelScope.launch(Dispatchers.IO) {
            try {
                dataBaseHandler.insertAsset(
                    Asset(
                        barcode = model.barCode,
                        category = model.category,
                        description = model.description
                    )
                )
                items.add(model)
                addAssetObserver.postValue(Event(true))
            } catch (e: Exception) {
                addAssetObserver.postValue(Event(false))
            }
        }
    }

    val fetchAssetsObserver = MutableLiveData<Event<ArrayList<AssetModel>>>()
    fun fetchAssets() {
        viewModelScope.launch(Dispatchers.IO) {
            items.clear()
            dataBaseHandler.getAllAssets().sortedBy { it.barcode }.forEach {
                items.add(
                    AssetModel(
                        barCode = it.barcode,
                        category = it.category,
                        description = it.description
                    )
                )
            }
            fetchAssetsObserver.postValue(Event(items))
        }
    }

    val editAssetObserver = MutableLiveData<Event<Boolean>>()
    fun editAsset(model: AssetModel) {
        viewModelScope.launch(Dispatchers.IO) {
            try {
                dataBaseHandler.updateAsset(Asset(model.barCode, model.category, model.description))
                editAssetObserver.postValue(Event(true))
            } catch (e: Exception) {
                editAssetObserver.postValue(Event(false))
            }
        }
    }

}