package com.ntgtest.viewModel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.ntgtest.model.SignInModel
import com.ntgtest.model.SignUpModel
import com.ntgtest.model.db.DataBaseHandler
import com.ntgtest.model.db.users.User
import com.ntgtest.utils.Event
import com.ntgtest.view.sign_in.validation_type.SignInValidation
import com.ntgtest.view.sign_in.validation_type.SignUpValidation
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlin.Exception

class LogInViewModel(private val dataBaseHandler: DataBaseHandler) : ViewModel() {

    val submitSignInModelValidator = MutableLiveData<Event<SignInValidation>>()

    fun isValidSignIn(signInModel: SignInModel) {
        var isValid = true
        if (signInModel.userName.isNullOrBlank()) {
            submitSignInModelValidator.value = Event(SignInValidation.USERNAME_REQUIRED)
            isValid = false
        }
        if (signInModel.password.isNullOrBlank()) {
            submitSignInModelValidator.value = Event(SignInValidation.PASSWORD_REQUIRED)
            isValid = false
        }
        if (isValid)
            userAuth(signInModel)
    }

    private fun userAuth(signInModel: SignInModel) {
        viewModelScope.launch(Dispatchers.IO) {
            signInModel.userName?.let {
                val auth = dataBaseHandler.loadUserByUsername(signInModel.userName)
                if (auth.isNullOrEmpty())
                    submitSignInModelValidator.postValue(
                        Event(SignInValidation.UNAUTHORIZED_CREDENTIALS))
                else {
                    if (auth.filter { it.password == signInModel.password }.isNullOrEmpty())
                        submitSignInModelValidator.postValue(
                            Event(SignInValidation.UNAUTHORIZED_CREDENTIALS))
                    else
                        submitSignInModelValidator.postValue(Event(SignInValidation.VALID))
                }
            }
        }
    }

    val signUpValidation = MutableLiveData<Event<SignUpValidation>>()
    fun isValidSignUp(signUpModel: SignUpModel){
        var isValid = true
        if(signUpModel.username.isNullOrBlank()) {
            signUpValidation.value = Event(SignUpValidation.USERNAME_EMPTY)
            isValid = false
        }
        if(signUpModel.password.isNullOrBlank()) {
            signUpValidation.value = Event(SignUpValidation.PASSWORD_EMPTY)
            isValid = false
        }
        if(isValid){
            viewModelScope.launch(Dispatchers.IO) {
                if(!dataBaseHandler.loadUserByUsername(signUpModel.username).isNullOrEmpty())
                    signUpValidation.postValue(Event(SignUpValidation.USERNAME_EXISTS))
                else
                    signUpValidation.postValue(Event(SignUpValidation.VALID))
            }
        }
    }

    val signupObserver = MutableLiveData<Event<Boolean>>()
    fun signUp(signUpModel: SignUpModel) {
        try {
            viewModelScope.launch(Dispatchers.IO) {
                dataBaseHandler.signUp(
                    User(
                        username = signUpModel.username,
                        password = signUpModel.password
                    )
                )
                signupObserver.postValue(Event(true))
            }
        }catch (e: Exception){
            signupObserver.postValue(Event(false))
        }
    }

}