package com.ntgtest.model.db.asset

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update

@Dao
interface AssetDao {

    @Query(value = "SELECT * FROM Asset")
    suspend fun getAll(): List<Asset>

    @Query("SELECT * FROM Asset WHERE barcode IN (:barCode)")
    suspend fun loadAllByIds(barCode: Array<String>): List<Asset>

    @Insert
    suspend fun insert(vararg asset: Asset)

    @Insert
    suspend fun insertAll(asset: List<Asset>)

    @Update
    suspend fun update(model: Asset)
}