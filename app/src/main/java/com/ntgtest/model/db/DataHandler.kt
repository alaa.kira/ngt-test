package com.ntgtest.model.db

import androidx.annotation.WorkerThread
import com.ntgtest.model.db.asset.Asset
import com.ntgtest.model.db.users.User

interface DataHandler {

    //Users Endpoints
    suspend fun signUp(user: User)

    suspend fun loadUserByUsername(username:String): List<User>

    //Assets Endpoints
    suspend fun getAllAssets(): List<Asset>

    suspend fun insertAsset(asset: Asset)

    suspend fun insertAllAsset(asset: List<Asset>)

    suspend fun hasBarcode(barcode: String): Boolean

    @WorkerThread
    suspend fun updateAsset(model: Asset)


}