package com.ntgtest.model.db

import android.content.Context
import androidx.annotation.WorkerThread
import com.ntgtest.model.db.asset.Asset
import com.ntgtest.model.db.users.User

class DataBaseHandler(
    private val context: Context,
    private val databaseManager: DatabaseManager = DatabaseManager.getInstance(context)
) : DataHandler {

    //User Endpoint
    @WorkerThread
    override suspend fun signUp(user: User) {
        return databaseManager.userDao().createUser(user)
    }

    @WorkerThread
    override suspend fun loadUserByUsername(username: String): List<User> {
        return databaseManager.userDao().loadAllByUserName(username)
    }

    //Assets Endpoints

    @WorkerThread
    override suspend fun getAllAssets(): List<Asset> {
        return databaseManager.assetDao().getAll()
    }

    @WorkerThread
    override suspend fun insertAsset(asset: Asset) {
        return databaseManager.assetDao().insert(asset)
    }

    @WorkerThread
    override suspend fun insertAllAsset(asset: List<Asset>) {
        return databaseManager.assetDao().insertAll(asset)
    }

    @WorkerThread
    override suspend fun hasBarcode(barcode: String): Boolean {
        return databaseManager.assetDao().loadAllByIds(arrayOf(barcode)).isNotEmpty()
    }

    @WorkerThread
    override suspend fun updateAsset(model: Asset) {
        databaseManager.assetDao().update(model)
    }
}