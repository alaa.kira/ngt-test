package com.ntgtest.model.db.users

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query

@Dao
interface UserDao {

    @Query("SELECT * FROM User WHERE username = :name")
    fun loadAllByUserName(vararg name: String) : List<User>

    @Insert
    fun createUser(user: User)
}