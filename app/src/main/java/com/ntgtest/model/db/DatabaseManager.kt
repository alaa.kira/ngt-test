package com.ntgtest.model.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.ntgtest.model.db.asset.Asset
import com.ntgtest.model.db.asset.AssetDao
import com.ntgtest.model.db.users.User
import com.ntgtest.model.db.users.UserDao

@Database(entities = [Asset::class, User::class], version = 2)
abstract class DatabaseManager : RoomDatabase() {
    abstract fun assetDao(): AssetDao
    abstract fun userDao(): UserDao

    companion object {
        private var db: DatabaseManager? = null
        fun getInstance(context: Context): DatabaseManager {
            db?.let {
                return it
            }
            return Room.databaseBuilder(
                context,
                DatabaseManager::class.java,
                "AssetDatabase"
            ).fallbackToDestructiveMigration().build()
        }
    }

}