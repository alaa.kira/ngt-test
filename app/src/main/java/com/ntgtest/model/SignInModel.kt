package com.ntgtest.model

data class SignInModel(
    val userName: String?,
    val password: String?,
)
