package com.ntgtest.model

data class SignUpModel(
    val username: String,
    val password: String
)
