package com.ntgtest.model

data class AssetModel(
    var barCode: String,
    var category: String,
    var description: String,
)