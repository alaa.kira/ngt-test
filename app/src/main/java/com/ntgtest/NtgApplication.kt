package com.ntgtest

import android.app.Application
import android.content.Context
import com.ntgtest.model.db.DataBaseHandler
import com.ntgtest.model.db.asset.Asset
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext

class NtgApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        val sharedPreferences = getSharedPreferences("Asset_Name", Context.MODE_PRIVATE)
        if (!sharedPreferences.getBoolean("ASSETS_LOADED", false)) {
            insertAssets()
        }
    }

    private fun insertAssets() {
        val rows = arrayListOf<Asset>()
        rows.add(Asset(barcode = "000001", category = "Cars", description = "Mercedes"))
        rows.add(Asset(barcode = "000002", category = "Furniture", description = "meeting table"))
        rows.add(
            Asset(
                barcode = "000003",
                category = "Equipment",
                description = "Sharp Calculator"
            )
        )
        rows.add(
            Asset(
                barcode = "000004",
                category = "Electrical Equipment",
                description = "Compressor"
            )
        )
        rows.add(
            Asset(
                barcode = "000005",
                category = "Fire extinguisher",
                description = "Powder Fire extinguisher"
            )
        )
        rows.add(Asset(barcode = "000006", category = "Telephone", description = "Optas Telephone"))
        rows.add(Asset(barcode = "000007", category = "Printer", description = "Samsung Printer"))
        rows.add(Asset(barcode = "000008", category = "Fax", description = "MoneyGram Fax"))
        rows.add(
            Asset(
                barcode = "000009",
                category = "Photocopier Machine",
                description = "Beta Machine"
            )
        )
        rows.add(
            Asset(
                barcode = "000010",
                category = "Calculator",
                description = "Cassio Calculator"
            )
        )
        rows.add(Asset(barcode = "000011", category = "Furniture", description = "work station"))
        rows.add(Asset(barcode = "000012", category = "Cars", description = "Volks Wagen"))
        rows.add(
            Asset(
                barcode = "000013",
                category = "Electrical Equipment",
                description = "Volks Wagen"
            )
        )
        rows.add(
            Asset(
                barcode = "000014",
                category = "Electrical Equipment",
                description = "Attendance Machine"
            )
        )
        rows.add(
            Asset(
                barcode = "000015",
                category = "Television & Video",
                description = "LG LCD 32"
            )
        )
        rows.add(
            Asset(
                barcode = "000016",
                category = "Alarm System",
                description = "Smoke Detector"
            )
        )
        rows.add(Asset(barcode = "000017", category = "Computer Machine", description = "CISCO PC"))
        rows.add(Asset(barcode = "000018", category = "Computer Machine", description = "HP PC"))
        rows.add(Asset(barcode = "000019", category = "Printer", description = "HP Printer"))
        rows.add(
            Asset(
                barcode = "000020",
                category = "Planet Machine",
                description = "Planet Machine"
            )
        )
        rows.add(
            Asset(
                barcode = "000021",
                category = "Metal Curtain",
                description = "Metal Curtain"
            )
        )
        rows.add(Asset(barcode = "000022", category = "Curtain", description = "White Curtain"))
        rows.add(
            Asset(
                barcode = "000023",
                category = "Desks & Tables",
                description = "Meeting Table 220x110x72.2"
            )
        )
        rows.add(Asset(barcode = "000024", category = "Desks & Tables", description = "Side Table"))
        rows.add(
            Asset(
                barcode = "000025",
                category = "Big Wooden Cabinet",
                description = "Cabinet"
            )
        )
        val dataBaseHandler = DataBaseHandler(baseContext)
        object : CoroutineScope {
            override val coroutineContext: CoroutineContext
                get() = Dispatchers.IO
        }.launch {
            dataBaseHandler.insertAllAsset(rows)
            getSharedPreferences("Asset_Name", Context.MODE_PRIVATE).edit()
                .putBoolean("ASSETS_LOADED", true).apply()
        }
    }
}