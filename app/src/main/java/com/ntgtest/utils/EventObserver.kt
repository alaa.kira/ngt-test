package com.ntgtest.utils

import androidx.lifecycle.Observer

class EventObserver<T>(var content: EventUnhandledContent<T>?) : Observer<Event<T>> {

    interface EventUnhandledContent<T> {
        fun onEventUnhandledContent(event: T)
    }

    override fun onChanged(event: Event<T>?) {
        if (event != null) {
            val result = event.getContentIfNotHandled()
            if (result != null && content != null) {
                content!!.onEventUnhandledContent(result)
            }
        }
    }
}