package com.ntgtest.view.sign_in

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.NavDirections
import androidx.navigation.fragment.findNavController
import com.google.android.material.snackbar.Snackbar
import com.ntgtest.R
import com.ntgtest.databinding.FragmentSignInBinding
import com.ntgtest.model.SignInModel
import com.ntgtest.model.SignUpModel
import com.ntgtest.model.db.DataBaseHandler
import com.ntgtest.utils.EventObserver
import com.ntgtest.view.sign_in.validation_type.SignInValidation
import com.ntgtest.view.sign_in.validation_type.SignUpValidation
import com.ntgtest.viewModel.LogInViewModel

/**
 * A simple [Fragment] subclass as the default destination in the navigation.
 */
class SignInFragment : Fragment() {

    private var _binding: FragmentSignInBinding? = null

    private var viewModel: LogInViewModel? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentSignInBinding.inflate(inflater, container, false)
        viewModel = LogInViewModel(DataBaseHandler(requireContext()))
        setObservers()
        return binding.root
    }

    private fun setObservers() {
        viewModel?.submitSignInModelValidator?.observe(viewLifecycleOwner, EventObserver(
            object : EventObserver.EventUnhandledContent<SignInValidation> {
                override fun onEventUnhandledContent(event: SignInValidation) {
                    when (event) {
                        SignInValidation.USERNAME_REQUIRED -> binding.userNameEditText.error =
                            getString(R.string.this_field_is_required)
                        SignInValidation.PASSWORD_REQUIRED -> binding.passwordEditText.error =
                            getString(R.string.this_field_is_required)
                        SignInValidation.VALID -> {
                            findNavController().navigate(object : NavDirections {
                                override val actionId: Int get() = R.id.action_log_in_to_assets_list
                                override val arguments: Bundle get() = Bundle()
                            })
                        }
                        SignInValidation.UNAUTHORIZED_CREDENTIALS -> {
                            binding.userNameEditText.error =
                                getString(R.string.wrong_credentials)
                            binding.passwordEditText.error =
                                getString(R.string.wrong_credentials)
                        }
                    }
                }
            }
        ))
        viewModel?.signUpValidation?.observe(viewLifecycleOwner, EventObserver(
            object : EventObserver.EventUnhandledContent<SignUpValidation>{
                override fun onEventUnhandledContent(event: SignUpValidation) {
                    when(event){
                        SignUpValidation.USERNAME_EXISTS -> binding.userNameEditText.error = getString(R.string.username_already_exist)
                        SignUpValidation.USERNAME_EMPTY -> binding.userNameEditText.error = getString(R.string.this_field_is_required)
                        SignUpValidation.PASSWORD_EMPTY -> binding.passwordEditText.error = getString(R.string.this_field_is_required)
                        SignUpValidation.VALID -> viewModel?.signUp(SignUpModel(username = binding.userNameEditText.text.toString(), binding.passwordEditText.text.toString()))
                    }
                }
            }
        ))
        viewModel?.signupObserver?.observe(viewLifecycleOwner, EventObserver(
            object : EventObserver.EventUnhandledContent<Boolean>{
                override fun onEventUnhandledContent(event: Boolean) {
                    when(event){
                        true -> findNavController().navigate(object : NavDirections {
                            override val actionId: Int get() = R.id.action_log_in_to_assets_list
                            override val arguments: Bundle get() = Bundle()
                        })
                        false -> Snackbar.make(requireView(), "Something went wrong",Snackbar.LENGTH_LONG).show()
                    }
                }
            }
        ))
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.submitBtn.setOnClickListener {
            viewModel?.isValidSignIn(
                SignInModel(
                    userName = binding.userNameEditText.text.toString(),
                    password = binding.passwordEditText.text.toString(),
                )
            )
        }
        binding.submitSignupBtn.setOnClickListener {
            viewModel?.isValidSignUp(SignUpModel(
                username = binding.userNameEditText.text.toString(),
                password = binding.passwordEditText.text.toString(),
            ))
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}