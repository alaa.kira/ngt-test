package com.ntgtest.view.sign_in.validation_type

enum class SignUpValidation {
    USERNAME_EXISTS, USERNAME_EMPTY, PASSWORD_EMPTY, VALID
}