package com.ntgtest.view.sign_in.validation_type

enum class SignInValidation {
    USERNAME_REQUIRED, PASSWORD_REQUIRED,UNAUTHORIZED_CREDENTIALS, VALID
}