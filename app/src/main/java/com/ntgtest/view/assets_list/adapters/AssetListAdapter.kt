package com.ntgtest.view.assets_list.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.ntgtest.databinding.AssetListItemBinding
import com.ntgtest.model.AssetModel

class AssetListAdapter :
    ListAdapter<AssetModel, AssetListAdapter.MyViewHolder>(DiffCallsBacks()) {

    var listener: MyListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = AssetListItemBinding.inflate(layoutInflater, parent, false)
        return MyViewHolder(binding)
    }

    inner class MyViewHolder(private val binding: AssetListItemBinding) :
        RecyclerView.ViewHolder(binding.root), View.OnClickListener {

        fun bind(item: AssetModel) {
            binding.item = item
            binding.executePendingBindings()
        }

        init {
            itemView.setOnClickListener(this)
        }

        override fun onClick(p0: View?) {
            listener?.onRowClicked(getItem(adapterPosition))
        }

    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    fun filterOf(query: String, items: MutableList<AssetModel>) {
        val result = items.filter { it.barCode.contains(query.lowercase()) }
        submitList(result)
    }

    class DiffCallsBacks : DiffUtil.ItemCallback<AssetModel>() {
        override fun areItemsTheSame(oldItem: AssetModel, newItem: AssetModel): Boolean {
            return oldItem.barCode == newItem.barCode
        }

        override fun areContentsTheSame(oldItem: AssetModel, newItem: AssetModel): Boolean {
            return oldItem == newItem
        }

    }

    interface MyListener{
        fun onRowClicked(item: AssetModel)
    }

}