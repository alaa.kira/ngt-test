package com.ntgtest.view.assets_list.validation

enum class AssetValidation {
    BARCODE_NOT_UNIQUE, BARCODE_EMPTY, CATEGORY_EMPTY, DESCRIPTION_EMPTY, VALID
}