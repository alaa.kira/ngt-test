package com.ntgtest.view.assets_list

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.doAfterTextChanged
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.Fragment
import com.google.android.material.snackbar.Snackbar
import com.google.zxing.integration.android.IntentIntegrator
import com.ntgtest.R
import com.ntgtest.databinding.AddAssetDialogBinding
import com.ntgtest.databinding.AssetDetailsDialogBinding
import com.ntgtest.databinding.FragmentAssetsListBinding
import com.ntgtest.model.AssetModel
import com.ntgtest.model.db.DataBaseHandler
import com.ntgtest.utils.EventObserver
import com.ntgtest.view.MainActivity
import com.ntgtest.view.assets_list.adapters.AssetListAdapter
import com.ntgtest.view.assets_list.validation.AssetValidation
import com.ntgtest.viewModel.AssetListViewModel

class AssetsListFragment : Fragment(), AssetListAdapter.MyListener {

    private var _binding: FragmentAssetsListBinding? = null

    private val binding get() = _binding!!

    private lateinit var adapterAsset: AssetListAdapter
    private var viewModel: AssetListViewModel? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentAssetsListBinding.inflate(inflater, container, false)
        viewModel = AssetListViewModel(DataBaseHandler(requireContext()))
        (requireActivity() as AppCompatActivity).supportActionBar?.show()
        adapterAsset = AssetListAdapter()
        adapterAsset.submitList(viewModel?.items ?: arrayListOf())
        viewModel?.fetchAssets()
        adapterAsset.listener = this
        binding.assetsListView.adapter = adapterAsset
        return binding.root
    }

    @SuppressLint("NotifyDataSetChanged")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        binding.searchField.doOnTextChanged { text, _, _, _ ->
            adapterAsset.filterOf((text ?: "").toString(), viewModel?.items ?: arrayListOf())
        }
        binding.searchField.doAfterTextChanged {
            adapterAsset.notifyDataSetChanged()
        }
        binding.cameraIcn.setOnClickListener {
            IntentIntegrator(requireActivity()).setOrientationLocked(true).setPrompt("Scan QR")
                .initiateScan()
            (requireActivity() as MainActivity).capturedBarcode.removeObservers(viewLifecycleOwner)
            (requireActivity() as MainActivity).capturedBarcode.observe(
                viewLifecycleOwner,
                EventObserver(
                    object : EventObserver.EventUnhandledContent<String> {
                        override fun onEventUnhandledContent(event: String) {
                            binding.searchField.setText(event)
                        }
                    }
                ))
        }
        binding.addAssetFab.setOnClickListener {
            addAssetDialog()
        }
        setObservers()
        super.onViewCreated(view, savedInstanceState)
    }

    private fun setObservers() {
        viewModel?.addAssetObserver?.observe(viewLifecycleOwner, EventObserver(
            object : EventObserver.EventUnhandledContent<Boolean> {
                override fun onEventUnhandledContent(event: Boolean) {
                    binding.loadingLayout.root.visibility = View.GONE
                    when (event) {
                        true -> {
                            viewModel?.fetchAssets()
                            Snackbar.make(
                                binding.root,
                                "Asset added Successfully",
                                Snackbar.LENGTH_LONG
                            ).show()
                        }
                        false -> Snackbar.make(
                            binding.root,
                            "Could not add asset",
                            Snackbar.LENGTH_LONG
                        ).show()
                    }
                }
            }
        ))
        viewModel?.fetchAssetsObserver?.observe(viewLifecycleOwner, EventObserver(
            object : EventObserver.EventUnhandledContent<ArrayList<AssetModel>> {
                @SuppressLint("NotifyDataSetChanged")
                override fun onEventUnhandledContent(event: ArrayList<AssetModel>) {
                    adapterAsset.submitList(event)
                    adapterAsset.notifyDataSetChanged()
                }
            }
        ))
        viewModel?.editAssetObserver?.observe(viewLifecycleOwner, EventObserver(
            object : EventObserver.EventUnhandledContent<Boolean> {
                override fun onEventUnhandledContent(event: Boolean) {
                    when (event) {
                        true -> viewModel?.fetchAssets()
                        false -> Snackbar.make(
                            binding.root,
                            "Could not add asset",
                            Snackbar.LENGTH_LONG
                        ).show()
                    }
                }
            }
        ))
        viewModel?.addAssetValidation?.observe(viewLifecycleOwner, EventObserver(
            object : EventObserver.EventUnhandledContent<ArrayList<AssetValidation>> {
                override fun onEventUnhandledContent(event: ArrayList<AssetValidation>) {
                    if (event.isNotEmpty())
                        binding.loadingLayout.root.visibility = View.GONE
                    val messageBuilder = StringBuilder("")
                    event.forEach {
                        messageBuilder.append(it.name + "\n")
                    }
                    if (event.isNotEmpty()) {
                        val builder = AlertDialog.Builder(requireContext())
                        builder.setMessage(messageBuilder.toString())
                        val dialog = builder.create()
                        builder.setPositiveButton("Ok") { _, _ -> dialog.cancel() }
                        builder.show()
                    }
                }
            }
        ))
    }

    private fun addAssetDialog() {
        val dialogBinding = AddAssetDialogBinding.inflate(layoutInflater)
        val builder = Dialog(requireContext())
        builder.setTitle(getString(R.string.add_asset))
        builder.setCancelable(true)
        val model = AssetModel("", "", "")
        dialogBinding.item = model
        (requireActivity() as MainActivity).capturedBarcode.removeObservers(viewLifecycleOwner)
        (requireActivity() as MainActivity).capturedBarcode.observe(viewLifecycleOwner,
            EventObserver(
                object : EventObserver.EventUnhandledContent<String> {
                    override fun onEventUnhandledContent(event: String) {
                        dialogBinding.barCodeField.setText(event)
                    }
                }
            ))
        dialogBinding.cameraIcn.setOnClickListener {
            IntentIntegrator(requireActivity()).setOrientationLocked(true).setPrompt("Scan QR").initiateScan()
        }
        dialogBinding.submitBtn.setOnClickListener {
            binding.loadingLayout.root.visibility = View.VISIBLE
            viewModel?.validateAsset(model)
            builder.cancel()
        }
        builder.setOnCancelListener {
            (requireActivity() as MainActivity).capturedBarcode.removeObservers(
                viewLifecycleOwner
            )
        }
        builder.setContentView(dialogBinding.root)
        builder.create()
        builder.show()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onRowClicked(item: AssetModel) {
        showAssetDialog(item)
    }

    private fun showAssetDialog(item: AssetModel) {
        val binding = AssetDetailsDialogBinding.inflate(layoutInflater)
        val builder = Dialog(requireContext())
        builder.setTitle(item.barCode)
        builder.setCancelable(true)
        binding.item = item
        binding.editCategoryIcn.setOnClickListener {
            binding.descriptionField.isEnabled = false
            binding.categoryField.isEnabled = true
        }
        binding.editDescriptionIcn.setOnClickListener {
            binding.descriptionField.isEnabled = true
            binding.categoryField.isEnabled = false
        }
        builder.setOnCancelListener {
            viewModel?.items?.let {
                builder.cancel()
                viewModel?.editAsset(item)
            }
        }
        builder.setContentView(binding.root)
        builder.create()
        builder.show()
    }
}