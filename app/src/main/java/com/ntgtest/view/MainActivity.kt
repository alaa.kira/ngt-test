package com.ntgtest.view

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.MutableLiveData
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import com.google.zxing.integration.android.IntentIntegrator
import com.ntgtest.R
import com.ntgtest.databinding.ActivityMainBinding
import com.ntgtest.utils.Event

class MainActivity : AppCompatActivity() {

    private lateinit var appBarConfiguration: AppBarConfiguration
    var capturedBarcode = MutableLiveData(Event(""))
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setSupportActionBar(binding.toolbar)

        val navController = findNavController(R.id.nav_host_fragment_content_main)
        appBarConfiguration = AppBarConfiguration(navController.graph)
        setupActionBarWithNavController(navController, appBarConfiguration)

        navController.addOnDestinationChangedListener { _, destination, _ ->
            toolbarVisibility(
                destination.id
            )
        }

    }

    private fun toolbarVisibility(id: Int) {
        when(id){
            R.id.Log_inFragment -> supportActionBar?.hide()
            else -> {
                supportActionBar?.show()
            }
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        val intentIntegrator = IntentIntegrator.parseActivityResult(requestCode, resultCode, data)
        if (intentIntegrator != null) {
            if (intentIntegrator.contents != null) {
                capturedBarcode.value = Event(intentIntegrator.contents)
            } else {
                Toast.makeText(baseContext, "Canceled...", Toast.LENGTH_LONG).show()
            }
        }
    }

}